﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Vegas_T3.WEB.DB;
using Vegas_T3.WEB.Models;
using Vegas_T3.WEB.Servicios;

namespace Vegas_T3.WEB.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly AppPruebaContext app;
        public HomeController(AppPruebaContext app)
        {
            this.app = app;
        }
        public UsuariosH ObtenerUsuario(string usuario, string contrasena)
        {
            UsuariosH usuarioo = app.Usuarioss.Where(o => o.Usuario == usuario && o.contrasena == contrasena).FirstOrDefault();
            if (usuarioo == null)
                return null;
            return usuarioo;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = app.Usuarioss.Where(o => o.Usuario == claim.Value).FirstOrDefault();
            ViewBag.Lista = app.Historias.Where(o => o.idUsuario == user.idUsuario).ToList();
            return View();
        }

        [HttpGet]
        public IActionResult AddHistoria()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddHistoria(Historia model)
        {
            if (ModelState.IsValid)
            {
                var historia = new Historia
                {
                    idUsuario = model.idUsuario,
                    codigoRegistro = model.codigoRegistro,
                    nombreMascota = model.nombreMascota,
                    fechaRegistro = model.fechaRegistro,
                    fechaNacimiento = model.fechaNacimiento,
                    sexo = model.sexo,
                    especie = model.especie,
                    raza = model.raza,
                    tamano = model.tamano,
                    datosParticulares = model.datosParticulares,
                    nombrePropietario = model.nombrePropietario,
                    direccion = model.direccion,
                    telefono = model.telefono,
                    email = model.email
                };
                app.Historias.Add(historia);
                app.SaveChanges();
                return Redirect("/Home/Index");
            }
            return View(model);
        }
    }
}
