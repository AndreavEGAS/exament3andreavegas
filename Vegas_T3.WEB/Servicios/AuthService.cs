﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vegas_T3.WEB.DB;
using Vegas_T3.WEB.Models;

namespace Vegas_T3.WEB.Servicios
{
    public interface IAuthService
    {
        UsuariosH ObtenerUsuario(string username, string password);
    }

    public class AuthService: IAuthService
    {
        private readonly AppPruebaContext app;
        public AuthService(AppPruebaContext app)
        {
            this.app = app;
        }
        public UsuariosH ObtenerUsuario(string usuario, string contrasena)
        {
            UsuariosH usuarioo = app.Usuarioss.Where(o => o.Usuario == usuario && o.contrasena == contrasena).FirstOrDefault();
            if (usuarioo == null)
                return null;
            return usuarioo;
        }
    }
}
