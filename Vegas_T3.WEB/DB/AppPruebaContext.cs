﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vegas_T3.WEB.Models;

namespace Vegas_T3.WEB.DB
{
    public class AppPruebaContext : DbContext
    {
        public DbSet<Historia> Historias { get; set; }
        public DbSet<UsuariosH> Usuarioss { get; set; }
        public AppPruebaContext(DbContextOptions<AppPruebaContext> options)
           : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new HistoriaConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());

            modelBuilder.Entity<Historia>().HasIndex(o => o.codigoRegistro).IsUnique();
        }
    }
}
