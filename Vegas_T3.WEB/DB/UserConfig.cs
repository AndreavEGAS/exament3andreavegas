﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vegas_T3.WEB.Models;

namespace Vegas_T3.WEB.DB
{
    public class UserConfig : IEntityTypeConfiguration<UsuariosH>
    {
        public void Configure(EntityTypeBuilder<UsuariosH> builder)
        {
            builder.ToTable("UsuariosH");
            builder.HasKey(o => o.idUsuario);
        }
    }
}
