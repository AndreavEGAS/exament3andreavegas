﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Vegas_T3.WEB.Models
{
    public class Historia
    {
        public int idHistoria { get; set; }
        [Required]
        public string codigoRegistro { get; set; }
        [Required]
        public string nombreMascota { get; set; }
        [Required]
        public DateTime fechaRegistro { get; set; }
        [Required]
        public DateTime fechaNacimiento { get; set; }
        public string sexo { get; set; }
        public string especie { get; set; }
        public string raza { get; set; }
        public string tamano { get; set; }
        public string datosParticulares { get; set; }
        [Required]
        public string nombrePropietario { get; set; }
        [Required]
        public string direccion { get; set; }
        [Required]
        public string telefono { get; set; }
        [Required]
        [EmailAddress]
        public string email { get; set; }
        public int idUsuario { get; set; }
    }
}
