USE [master]
GO
/****** Object:  Database [Mascotas]    Script Date: 04/11/2020 9:49:41 ******/
CREATE DATABASE [Mascotas]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Mascotas', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Mascotas.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Mascotas_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Mascotas_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Mascotas] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Mascotas].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Mascotas] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Mascotas] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Mascotas] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Mascotas] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Mascotas] SET ARITHABORT OFF 
GO
ALTER DATABASE [Mascotas] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Mascotas] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Mascotas] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Mascotas] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Mascotas] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Mascotas] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Mascotas] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Mascotas] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Mascotas] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Mascotas] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Mascotas] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Mascotas] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Mascotas] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Mascotas] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Mascotas] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Mascotas] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Mascotas] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Mascotas] SET RECOVERY FULL 
GO
ALTER DATABASE [Mascotas] SET  MULTI_USER 
GO
ALTER DATABASE [Mascotas] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Mascotas] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Mascotas] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Mascotas] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Mascotas] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Mascotas] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Mascotas', N'ON'
GO
ALTER DATABASE [Mascotas] SET QUERY_STORE = OFF
GO
USE [Mascotas]
GO
/****** Object:  Table [dbo].[Historia]    Script Date: 04/11/2020 9:49:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Historia](
	[idHistoria] [int] IDENTITY(1,1) NOT NULL,
	[codigoRegistro] [varchar](50) NULL,
	[nombreMascota] [varchar](50) NULL,
	[fechaRegistro] [date] NULL,
	[fechaNacimiento] [date] NULL,
	[sexo] [varchar](50) NULL,
	[especie] [varchar](50) NULL,
	[raza] [varchar](50) NULL,
	[tamano] [varchar](50) NULL,
	[datosParticulares] [varchar](50) NULL,
	[nombrePropietario] [varchar](50) NULL,
	[direccion] [varchar](50) NULL,
	[telefono] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[idUsuario] [int] NULL,
 CONSTRAINT [PK_Historia] PRIMARY KEY CLUSTERED 
(
	[idHistoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuariosH]    Script Date: 04/11/2020 9:49:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuariosH](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](50) NULL,
	[contrasena] [varchar](50) NULL,
 CONSTRAINT [PK_UsuariosH] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Historia]  WITH CHECK ADD  CONSTRAINT [FK_Historia_UsuariosH] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[UsuariosH] ([idUsuario])
GO
ALTER TABLE [dbo].[Historia] CHECK CONSTRAINT [FK_Historia_UsuariosH]
GO
USE [master]
GO
ALTER DATABASE [Mascotas] SET  READ_WRITE 
GO
