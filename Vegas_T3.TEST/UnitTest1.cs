using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Vegas_T3.WEB.Controllers;
using Vegas_T3.WEB.Models;
using Vegas_T3.WEB.Servicios;

namespace Vegas_T3.TEST
{
    public class Tests
    {
        [Test]
        public void LoginRetornaVista()
        {
            var controller = new AuthController(null);
            var view = controller.Login();
            Assert.IsInstanceOf<ViewResult>(view);
        }

        [Test]
        public void LoginMAL()
        {
            var serviceAuthManagerMock = new Mock<IAuthService>();
            serviceAuthManagerMock.Setup(u => u.ObtenerUsuario("admin", "admin")).Returns(new UsuariosH() { idUsuario = 1 });

            var controller = new AuthController(serviceAuthManagerMock.Object);
            var view = controller.Login("", "");
            Assert.IsInstanceOf<ViewResult>(view);
        }
        [Test]
        public void HomeVista()
        {
            var controller = new HomeController(null);
            var view = controller.AddHistoria();
            Assert.IsInstanceOf<ViewResult>(view);
        }


    }
}